from django.shortcuts import render, render_to_response, redirect 
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect 
from django.template import loader 
import ncs
import ipaddress
import json



def open_session(request):
    session_username = request.session['username']
    session_context = 'Django Portal'
    m = ncs.maapi.Maapi()
    s = ncs.maapi.Session(m, session_username, session_context)
    #  s = ncs.maapi.Session(m, 'NSO', 'orange')
    t = m.start_write_trans()
    
    return t

def checksession(request):
    #check if the user login and have session
    if 'username' not in request.session:
        return False

def get_device(request):
    DevicesList = []
    t = open_session(request)
    root = ncs.maagic.get_root(t)
    for box in root.devices.device:
        DevicesList.append(box.name)
    # return DevicesList
    data = {
            'devices': DevicesList
        }
    # return JsonResponse(data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def get_devices(request):
    """
    Use a MAAPI session via maagic api to get Devices List
    We do this by:
    1. Creating a NSO session  by calling open_session Function
    2. Create a pointer to  Devices
    3. Return Devices List
    """
    DevicesList = []
    t = open_session(request)
    root = ncs.maagic.get_root(t)
    for box in root.devices.device:
        DevicesList.append(box.name)
    return DevicesList

# rendering 



def newPrefixSet(request):
    forms = json.loads(request.GET['data_form'])
    print(forms['data'])
    t = open_session(request)
    root = ncs.maagic.get_root(t)



    for form in forms['data']:
        device_name = form['device_name']
        prefix_name = form['prefix_name']
        prefix_ip = form['prefix_ip']
        comment = form['comment']
        error_msg=None

        root.devices.device[device_name].config.cisco_ios_xr__prefix_set.create(prefix_name)
        newpre = root.devices.device[device_name].config.cisco_ios_xr__prefix_set[prefix_name]
        new_comment=newpre.set.create('#'+comment)
        for p in prefix_ip:
            new_ip=newpre.set.create(p)
     
        
    try:
        t.apply()
        result=True
    except Exception as e:
        print(str(e))
        result=False
        error_msg=str(e)

    if (result == True):
        data = {
        'result': True,
        'error_msg':error_msg
        }
    else:
         data = {
        'result': False,
        'error_msg':error_msg
        }
    return JsonResponse(data)



def get_prefixName(request):
    """
    Use a MAAPI session via maagic api to get all logical system .
    Uses the device name  an input parameter in the request .
    Return in Json object contain all logical system names .
    We do this by:
    1. Creating a NSO session  by calling open_session Function
    2. Create a pointer to  logical system in this  device
    3. loop to get all logical system names and append in logSys_list
    4. set the output variable to the Data  and  return the Respond in Json object
    """
  
    prefix_name_list = []
    device_name = request.GET.get('device_name' , None )
    t = open_session(request)
    root = ncs.maagic.get_root(t)
    for device in root.devices.device:
        if (device.name == device_name):
            for prefix in device.config.cisco_ios_xr__prefix_set:
                prefix_name_list.append(prefix.name)
    data = {
        'prefix_name_list': prefix_name_list
    }
    return JsonResponse(data)

      




    return render(request , 'PrefixSet/login.html')
